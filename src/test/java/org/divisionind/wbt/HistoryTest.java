/*
 * Copyright 2017-2018 Division Industries
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.divisionind.wbt;

import org.junit.Test;

import java.text.NumberFormat;

/**
 * Created by rhague on 9/25/2017.
 */
class HistoryTest {

    private long time;

    @Test
    void test() {
        System.out.println("Loading...");
        time = System.currentTimeMillis();
        History h = new History(20);
        for (int i = 0;i<25;i++)  {
            h.load("Page: " + i);
            try {
                Thread.sleep(2); // pages indexed by time(in ms) so a small delay is needed between loading
            } catch (InterruptedException e) {}
        }
        printTime();
        for (int i = 1;i<4;i++) System.out.println("Back(" + i + "): " + h.back());
        printTime();
        for (int i = 1;i<5;i++) System.out.println("Forward(" + i + "): " + h.forward());
        printTime();
        System.out.println("Back(1): " + h.back());
        printTime();
    }

    private void printTime() { // print and reset time
        System.out.println("=================================");
        System.out.println("  Done (" + NumberFormat.getNumberInstance().format(System.currentTimeMillis() - time) + "ms)");
        System.out.println("=================================");
        time = System.currentTimeMillis();
    }
}