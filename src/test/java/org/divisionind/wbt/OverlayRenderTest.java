/*
 * Copyright 2017-2018 Division Industries
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.divisionind.wbt;

import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;

/**
 * Created by rhague on 9/18/2017.
 */
class OverlayRenderTest {
    @Test
    void render() {
        OverlayRender o;
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Font f = null;
        for (Font ff : env.getAllFonts()) {
            if (ff.getName().equals("Agency FB")) {
                f = ff;
                break;
            }
        }

        if (f == null) return;
        long start = System.currentTimeMillis();

        o = new OverlayRender("This is a test",
                f.deriveFont(50f),
                new Color(0, 63, 255, 145),
                new Color(0, 0, 0),
                10, 10, 20, 20, 35);
        try {
            ImageIO.write(o.render(), "png", new File("C:\\Users\\rhague.PRCC\\IdeaProjects\\WebViewTouch\\tests\\out\\test.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Rendered in " + NumberFormat.getNumberInstance().format(System.currentTimeMillis() - start) + "ms");
    }

    @Test
    void listFonts() {
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        for (Font f : env.getAllFonts()) System.out.println(f.getFontName());
    }
}