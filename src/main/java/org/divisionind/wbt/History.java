/*
 * Copyright 2017-2018 Division Industries LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.divisionind.wbt;

import java.util.*;

/**
 * Created by drew6017 on 9/25/2017.
 */
@Deprecated
public class History {

    private int length;
    private HashMap<Long, String> h;
    private List<String> exclusion;
    private long on;

    public History(int length) {
        this.length = length;
        this.h = new HashMap<>(length + 1);
        this.exclusion = new ArrayList<>();
        this.on = 0;
    }

    public void load(String s) {
        if (exclusion.remove(s)) return;
        this.on = System.currentTimeMillis();
        this.h.put(on, s);
        if (h.size() >= length) {
            h.remove(Collections.min(h.keySet()));
        }
    }

    public String back() { // do not call load for the page of these functions
        Long[] oa = h.keySet().toArray(new Long[h.size()]);
        Arrays.sort(oa, Collections.reverseOrder());
        for (long k : oa) {
            if (k < on) {
                this.on = k;
                String v = h.get(k);
                exclude(v);
                return v;
            }
        }
        return null;
    }

    public String forward() { // do not call load for the page of these functions
        Long[] ks = h.keySet().toArray(new Long[h.size()]);
        Arrays.sort(ks);
        for (long k : ks) {
            if (k > on) {
                this.on = k;
                String v = h.get(k);
                exclude(v);
                return v;
            }
        }
        return null;
    }

    public void reset() {
        h.clear();
    }

    public void exclude(String x) {
        exclusion.add(x);
    }
}
