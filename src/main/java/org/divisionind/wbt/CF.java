/*
 * Copyright 2017-2018 Division Industries LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.divisionind.wbt;

import com.divisionind.divml.YamlConfiguration;
import org.divisionind.wbt.util.AtomicScheduler;

import javax.swing.*;
import java.io.*;
import java.util.Map;

/**
 * Created by drew6017 on 9/20/2017.
 */
public class CF { // stands for ConfigFields

    public static String USER_AGENT;
    public static String LOAD_SITE;
    public static String COM_PORT;
    public static boolean NO_CURSOR;
    public static boolean DO_COM_INIT;
    public static int TIMEOUT; // minutes
    //button
    public static int OVERLAY_DURATION;
    public static int FADE_DURATION;
    public static Overlay.Location DISPLAY_LOCATION;

    public static boolean ON_SCREEN_BUTTONS;
    public static boolean ON_SCREEN_LAYOUT;
    public static int ON_SCREEN_BUTTONS_POS;
    public static double ON_SCREEN_BUTTONS_SCALE;
    public static long ON_SCREEN_BUTTON_PRESS_DELAY;

    public static float SCROLL_SPECTRAL_DECAY;
    public static float SCROLL_STOP_MAGNITUDE;
    public static boolean SCROLL_USE;

    public static AtomicScheduler scheduler;

    static {
        scheduler = new AtomicScheduler(2);
        init(); // initialization code
    }

    private static void init() {
        File config = new File(new File("").getAbsoluteFile(), "config.yml");
        if (!config.exists()) {
            try {
                extractResource("/org/divisionind/wbt/assets/config.yml", config, 2048);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Error extracting config resource.", "IO Exception", JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
                System.exit(-1);
                return;
            }
            JOptionPane.showMessageDialog(null, "No configuration found. A new one has been extracted,\nplease modify it (if needed) and restart the application.", "No config", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
            return;
        }
        try {
            YamlConfiguration c = YamlConfiguration.load(config);
            USER_AGENT = c.getString("UserAgent");
            LOAD_SITE = c.getString("MainPage");
            COM_PORT = c.getString("COMPort");
            NO_CURSOR = (boolean)c.get("NoCursor");
            DO_COM_INIT = (boolean)c.get("DoCOMInit");

            ON_SCREEN_BUTTONS = (boolean)c.get("OnScreenButtons");
            ON_SCREEN_LAYOUT = (boolean)c.get("OnScreenLayout");
            ON_SCREEN_BUTTONS_SCALE = (double)c.get("OnScreenButtonsScale");
            ON_SCREEN_BUTTONS_POS = (int)c.get("OnScreenButtonsPosY");
            ON_SCREEN_BUTTON_PRESS_DELAY = (long)(int)c.get("OnScreenButtonPressDelay");

            TIMEOUT = c.getInteger("Timeout") * 60;
            //Run.history = new History(c.getInteger("HistoryLength"));
            Map<String, Object> scroll = c.getAsMap("Scroll");
            SCROLL_USE = (boolean)scroll.get("Use");
            SCROLL_SPECTRAL_DECAY = (float)(double)scroll.get("SpectralDecay");
            SCROLL_STOP_MAGNITUDE = (float)(double)scroll.get("StopMagnitude");

            Map<String, Object> button = c.getAsMap("Button");
            OVERLAY_DURATION = (int)button.get("OverlayDuration");
            FADE_DURATION = (int)button.get("FadeDuration");
            DISPLAY_LOCATION = Overlay.Location.valueOf((String)button.get("DisplayLocation"))
                    .setBottomOffset((int)button.get("BottomOffset"));

            Map<String, Object> bts = (Map<String, Object>)button.get("Buttons");
            Map<String, Object> overlays = c.getAsMap("Overlays");

            bts.forEach((k, v) -> loadButton((Map<String, Object>)v, k));

            overlays.forEach((k, v) -> {
                try {
                    loadImageOverlays((Map<String, Object>)v, k);
                } catch (IOException e) {
                    String msg = "Error loading the image for button: " + k;
                    System.out.println(msg);
                    JOptionPane.showMessageDialog(null, "Problem loading image file:\n" + e.getMessage() +
                            "\nPlease check the file and try again.", "Loading Error", JOptionPane.ERROR_MESSAGE);
                    e.printStackTrace();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Problem loading configuration file:\n" + e.getMessage() +
                    "\nPlease delete the file and run this program again.", "Configuration Error", JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
            return;
        }
    }

    private static void loadButton(Map<String, Object> button, String key) {
        int cooldown = (int)button.get("PressCooldown");
        Buttons.ButtonEvent be;
        switch (key) {
            case "Home":
                be = new Buttons.HomeButton(cooldown);
                break;
            case "Forward":
                be = new Buttons.ForwardButton(cooldown);
                break;
            case "Back":
                be = new Buttons.BackButton(cooldown);
                break;
            default:
                return;
        }

        Buttons.buttonEvents.put((int)button.get("Pin"), be);
    }

    private static void loadImageOverlays(Map<String, Object> sub, String on) throws IOException {
        Overlay o = Overlay.valueOf(on.toUpperCase());
        double scaledSize = (double)sub.get("Scale");
        String imgPath = (String)sub.get("OverlayImage");
        if (!imgPath.equals("DEFAULT")) o.setIcon(new File(imgPath));
        if (scaledSize != -1) o.setScaleTo(scaledSize);
    }

    private static void extractResource(String path, File out, int buffer_size) throws IOException {
        InputStream in = YamlConfiguration.class.getResourceAsStream(path);
        if(in == null) {
            throw new IOException("Internal resource not found.");
        } else {
            OutputStream o = new FileOutputStream(out);
            byte[] buff = new byte[buffer_size];
            int len;
            while((len = in.read(buff)) != -1) o.write(buff, 0, len);
            in.close();
            o.flush();
            o.close();
        }
    }
}
