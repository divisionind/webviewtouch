/*
 * Copyright 2017-2018 Division Industries LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.divisionind.wbt;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserType;
import com.teamdev.jxbrowser.chromium.javafx.BrowserView;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.embed.swing.JFXPanel;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.ImageCursor;
import javafx.scene.Scene;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseMotionListener;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.LogManager;

import static org.divisionind.wbt.CF.*;

/**
 * Created by drew6017 on 7/24/2017.
 */
public class Run {

    //public static WebEngine engine;
    public static AtomicInteger ai;
    public static Scroll scroll;

    public static Browser engine;
    public static BrowserView view;

    public static void main(String[] args) { // TODO Fix being able to go back before home (after new history impl) (remove initial "nothing" page from history)
        // fixes console pop up issue with adobe flash
        System.setProperty("jxbrowser.chromium.sandbox", "true");

        SwingUtilities.invokeLater(() -> {
            JWindow w = new JWindow(new JFrame() {
                public boolean isShowing() {
                    return true;
                }
            });
            JFXPanel p = new JFXPanel();
            ai = new AtomicInteger(TIMEOUT);
            try {
                GlobalScreen.registerNativeHook();
            } catch (NativeHookException e) {
                e.printStackTrace();
            }

            GlobalScreen.addNativeKeyListener(new NativeKeyListener() {
                @Override
                public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {}

                @Override
                public void nativeKeyPressed(NativeKeyEvent e) {}

                @Override
                public void nativeKeyReleased(NativeKeyEvent e) {
                    ai.set(TIMEOUT);
                    if (e.getKeyCode() == 1) System.exit(0);
                }
            });
            GlobalScreen.addNativeMouseMotionListener(new NativeMouseMotionListener() {
                @Override
                public void nativeMouseMoved(NativeMouseEvent nativeMouseEvent) {
                    ai.set(TIMEOUT);
                }

                @Override
                public void nativeMouseDragged(NativeMouseEvent nativeMouseEvent) {
                    ai.set(TIMEOUT);
                }
            });

            new Thread(() -> {
                while (true) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {}
                    if (ai.decrementAndGet() <= 0) {
                        ai.set(TIMEOUT);
                        if (!engine.getURL().equals(LOAD_SITE)) {
                            Platform.runLater(() -> engine.loadURL(LOAD_SITE));
                        }
                    }
                }
            }).start();

            LogManager.getLogManager().reset();
            w.add(p);
            w.pack();
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            w.setSize(screenSize);
            p.setSize(screenSize);
            w.getContentPane().setPreferredSize(screenSize);
            w.setLocation(0, 0);
            w.setVisible(true);

            Platform.runLater(() -> init(p));
        });
    }

    private static void init(JFXPanel p) {
        Group g = new Group();
        Scene s = new Scene(g);
        p.setScene(s);
        try {
            engine = new Browser(BrowserType.HEAVYWEIGHT);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        view = new BrowserView(engine);
        g.getChildren().add(view);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double h = screenSize.getHeight();
        double w = screenSize.getWidth();
        view.setMaxSize(w, h);
        view.setMinSize(w, h);
        view.setOnContextMenuRequested(Event::consume);
        //view.setContextMenuEnabled(false);
        String css;
        if (NO_CURSOR) {
            final Cursor blank = new ImageCursor(SwingFXUtils.toFXImage(new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB), null), 0, 0);
            view.cursorProperty().bind(Bindings.when(new SimpleBooleanProperty(true)).then(blank).otherwise(blank));
            css = "/org/divisionind/wbt/assets/internal.css";
        } else css = "/org/divisionind/wbt/assets/internal-wcursor.css";

        try {
            engine.setCustomStyleSheet(readInput("".getClass().getResourceAsStream(css)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //engine.setCustomStyleSheet("".getClass().getResource("/org/divisionind/wbt/assets/internal.css").toExternalForm());
        if (SCROLL_USE) {
            scroll = new Scroll();
            scroll.start();
        }

        engine.setUserAgent(USER_AGENT);
        //engine.addLoadListener(new LoadListener() {
        engine.loadURL(LOAD_SITE);
        if (DO_COM_INIT) Buttons.init();

        // create button overlay
        if (CF.ON_SCREEN_BUTTONS) {
            if (CF.ON_SCREEN_LAYOUT) {
                java.awt.EventQueue.invokeLater(() -> new ButtonHostUIVertical(CF.ON_SCREEN_BUTTONS_SCALE, CF.ON_SCREEN_BUTTONS_POS).setVisible(true));
            } else {
                java.awt.EventQueue.invokeLater(() -> new ButtonHostUI(CF.ON_SCREEN_BUTTONS_SCALE, CF.ON_SCREEN_BUTTONS_POS).setVisible(true));
            }
        }
    }

    private static String readInput(InputStream in) throws IOException {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        byte[] buff = new byte[2048];
        int len;
        while ((len = in.read(buff)) != -1) bao.write(buff, 0, len);
        in.close();
        bao.flush();
        bao.close();
        return bao.toString("UTF-8");
    }
}