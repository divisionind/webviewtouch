/*
 * Copyright 2017-2018 Division Industries LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.divisionind.wbt;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.concurrent.ScheduledFuture;

/**
 * Created by drew6017 on 9/19/2017.
 */
public enum Overlay {

    HOME("/org/divisionind/wbt/assets/home.png"),
    FORWARD("/org/divisionind/wbt/assets/forward.png"),
    BACK("/org/divisionind/wbt/assets/backward.png"),
    ERROR("/org/divisionind/wbt/assets/xcircle.png");

    private static JWindow showing = null;
    private static ScheduledFuture<?> fadeFuture;
    private static final int FADE_RESOLUTION = 100;
    private static final MathContext NO_PLACES = new MathContext(0, RoundingMode.UP);

    private ImageIcon icon;
    private String loc;

    Overlay(String loc) {
        this.loc = loc;
        try {
            this.icon = new ImageIcon(ImageIO.read(Overlay.class.getResourceAsStream(loc)));
        } catch (IOException e) {
            this.icon = null;
            System.err.println("Error at runtime: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public ImageIcon readNewIconInstance() {
        try {
            return new ImageIcon(ImageIO.read(Overlay.class.getResourceAsStream(loc)));
        } catch (IOException e) {
            return null;
        }
    }

    public Icon getScaledNewInstance(double scale) {
        BufferedImage img = (BufferedImage)readNewIconInstance().getImage();
        double screenH = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        double objH = screenH * scale / 100;
        double w = objH / (double)img.getHeight() * (double)img.getWidth();
        return new ImageIcon(img.getScaledInstance((int)w, (int)objH, Image.SCALE_AREA_AVERAGING));
    }

    public void setIcon(File f) throws IOException {
        BufferedImage img = ImageIO.read(f);
        if (img!=null) this.icon = new ImageIcon(img);
    }

    /*
    public void setScale(int scaledH) { // .222
        BufferedImage img = (BufferedImage)icon.getImage();
        int w = (int) ((double)scaledH / (double)img.getHeight() * (double)img.getWidth());
        this.icon = new ImageIcon(img.getScaledInstance(w, scaledH, Image.SCALE_AREA_AVERAGING));
    }
    */

    public void setScaleTo(double s) {
        BufferedImage img = (BufferedImage)icon.getImage();
        double screenH = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        double objH = screenH * s / 100;
        double w = objH / (double)img.getHeight() * (double)img.getWidth();
        this.icon = new ImageIcon(img.getScaledInstance((int)w, (int)objH, Image.SCALE_AREA_AVERAGING));
    }

    public void display(long displayTime, long fadeTime, Location loc) { // possibly add a move in from bottom effect
        stopShowing();
        showing = new JWindow();
        JLabel label = new JLabel();
        label.setIcon(icon);
        showing.add(label);
        showing.pack();
        showing.setBackground(new Color(0, 0, 0, 0));
        showing.setAlwaysOnTop(true);
        if (loc.equals(Location.CENTER)) showing.setLocationRelativeTo(null); else {
            Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
            int posx = middle(screen.width) - middle(icon.getIconWidth());
            int posy = screen.height - icon.getIconHeight() - loc.offset;
            showing.setLocation(posx, posy);
        }
        showing.setVisible(true);
        fadeFuture = CF.scheduler.delay(() -> fadeShowing(fadeTime), displayTime);
    }

    private int middle(int i) {
        return new BigDecimal(i).divide(new BigDecimal(2), NO_PLACES).intValue();
    }

    public static void stopShowing() {
        if (isShowing()) {
            showing.dispose();
            showing = null;
        }
        if (fadeFuture != null) fadeFuture.cancel(true);
    }

    public static void fadeShowing(long fadeTime) {
        long sleep = new BigDecimal(fadeTime).divide(new BigDecimal(FADE_RESOLUTION), new MathContext(0, RoundingMode.HALF_UP)).longValue();
        for (int i = FADE_RESOLUTION;i > -1; i--) {
            if (showing == null) break;
            if (i == 0) {
                stopShowing();
                break;
            }
            showing.setOpacity((float)i / (float)FADE_RESOLUTION);
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {}
        }
    }

    public static boolean isShowing() {
        return showing != null;
    }

    public enum Location {
        CENTER,
        BOTTOM_CENTER;

        protected int offset = 0; // default val

        public Location setBottomOffset(int offset) {
            this.offset = offset;
            return this;
        }
    }
}
