/*
 * Copyright 2017-2018 Division Industries LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.divisionind.wbt.util;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by drew6017 on 9/19/2017.
 */
public class AtomicScheduler {
    private ScheduledExecutorService pool;

    public AtomicScheduler(ScheduledExecutorService pool) {
        this.pool = pool;
    }

    public AtomicScheduler() {
        if(DaemonThreadFactory.getThreadFactory() == null) {
            DaemonThreadFactory.init();
        }

        this.pool = Executors.newSingleThreadScheduledExecutor(DaemonThreadFactory.getThreadFactory());
    }

    public AtomicScheduler(int i) {
        if(DaemonThreadFactory.getThreadFactory() == null) {
            DaemonThreadFactory.init();
        }

        this.pool = Executors.newScheduledThreadPool(i, DaemonThreadFactory.getThreadFactory());
    }

    public ScheduledFuture<?> delay(Runnable r, long delay, TimeUnit u) {
        return this.pool.schedule(r, delay, u);
    }

    public ScheduledFuture<?> delay(Runnable r, long delay) {
        return this.pool.schedule(r, delay, TimeUnit.MILLISECONDS);
    }

    public ScheduledFuture<?> repeating(Runnable r, long initialDelay, long period, TimeUnit u) {
        return this.pool.scheduleWithFixedDelay(r, initialDelay, period, u);
    }

    public ScheduledFuture<?> repeating(Runnable r, long initialDelay, long period) {
        return repeating(r, initialDelay, period, TimeUnit.MILLISECONDS);
    }

    public void shutdown(long i) {
        if(i == 0L) {
            this.pool.shutdownNow();
        }

        try {
            this.pool.shutdown();
            this.pool.awaitTermination(i, TimeUnit.MILLISECONDS);
        } catch (InterruptedException var7) { } finally {
            this.pool.shutdownNow();
        }
    }
}