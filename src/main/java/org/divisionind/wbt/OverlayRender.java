/*
 * Copyright 2017-2018 Division Industries LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.divisionind.wbt;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import static java.awt.RenderingHints.*;

/**
 * Created by drew6017 on 9/18/2017.
 */
@Deprecated
public class OverlayRender {

    private static OverlayRender displaying = null;

    private String msg;
    private Font font;
    private Color backgroundColor; // stores opacity
    private Color textColor;
    //private int duration;
    //private int fadeDuration;
    //private int x, y;
    private int padding_top, padding_bottom, padding_left, padding_right;
    private int cornerRounding;

    public OverlayRender(String msg, Font font, Color backgroundColor, Color textColor, int padding_top, int padding_bottom, int padding_left, int padding_right, int cornerRounding) {
        this.msg = msg;
        this.font = font;
        this.backgroundColor = backgroundColor;
        this.textColor = textColor;
        this.padding_top = padding_top;
        this.padding_bottom = padding_bottom;
        this.padding_left = padding_left;
        this.padding_right = padding_right;
        this.cornerRounding = cornerRounding;
    }

    public void display() {
        JWindow w = new JWindow();
        w.setAlwaysOnTop(true);
    }

    public BufferedImage render() {
        if (msg == null || msg.equals("")) return null;
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        BufferedImage i1 = new BufferedImage(screen.width, screen.height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D gi1 = i1.createGraphics();
        prep(gi1);
        Rectangle2D rec = font.getStringBounds(msg, gi1.getFontRenderContext());
        int textWidth = round(rec.getWidth());
        int textHeight = round(rec.getHeight());
        BufferedImage i2 = new BufferedImage(textWidth + padding_left + padding_right,
                textHeight + padding_top + padding_bottom,
                BufferedImage.TYPE_INT_ARGB);

        Graphics2D gi2 = i2.createGraphics();
        prep(gi2);

        gi2.setColor(backgroundColor);
        gi2.fillRoundRect(0, 0, i2.getWidth(), i2.getHeight(), cornerRounding, cornerRounding);

        gi2.setFont(font);
        gi2.setColor(textColor);
        gi2.drawString(msg, padding_left, textHeight + round((double)padding_top / 2D));

        return i2;
    }

    private void prep(Graphics2D g) {
        g.setRenderingHint(KEY_TEXT_ANTIALIASING, VALUE_TEXT_ANTIALIAS_ON);
        g.setRenderingHint(KEY_RENDERING, VALUE_RENDER_QUALITY);
        g.setRenderingHint(KEY_ALPHA_INTERPOLATION, VALUE_ALPHA_INTERPOLATION_QUALITY);
        g.setRenderingHint(KEY_COLOR_RENDERING, VALUE_COLOR_RENDER_QUALITY);
        g.setRenderingHint(KEY_FRACTIONALMETRICS, VALUE_FRACTIONALMETRICS_ON);
    }

    private int round(double d) {
        return new BigDecimal(d).round(new MathContext(0, RoundingMode.UP)).intValue();
    }
}
