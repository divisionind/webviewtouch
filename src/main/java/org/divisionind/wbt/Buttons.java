/*
 * Copyright 2017-2018 Division Industries LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.divisionind.wbt;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortPacketListener;
import javafx.application.Platform;

import javax.swing.*;
import java.util.HashMap;

/**
 * Created by drew6017 on 8/18/2017.
 */
public class Buttons {

    public static final HashMap<Integer, ButtonEvent> buttonEvents = new HashMap<>();

    public static void init() {
        System.out.println("Starting button events.");
        new Thread(() -> {
            while (true) {
                SerialPort p = SerialPort.getCommPort(CF.COM_PORT);
                p.setBaudRate(115200);
                if (p.openPort()) {
                    p.addDataListener(new PacketHandler());
                    System.out.println("Registered button handler.");
                    while (p.isOpen()) {
                        try {
                            Thread.sleep(1000L);
                        } catch (InterruptedException e) {}
                    }
                } else {
                    String msg = "Could not connect to device on port " + CF.COM_PORT;
                    System.err.println(msg);
                    JOptionPane.showMessageDialog(null, msg, "Error", JOptionPane.ERROR_MESSAGE);
                }
                try {
                    Thread.sleep(1000L); // poll 1x a second for a new home button device
                } catch (InterruptedException e) {}
            }
        }).start();
    }

    private static int unsign(byte b) {
        return b & 0xFF;
    }

    private static class PacketHandler implements SerialPortPacketListener {
        @Override
        public int getPacketSize() {
            return 2;
        }

        @Override
        public int getListeningEvents() {
            return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
        }

        @Override
        public void serialEvent(SerialPortEvent ev) {
            byte[] buff = ev.getReceivedData();
            ButtonEvent event = buttonEvents.get(unsign(buff[0]));
            if (event == null) {
                System.err.println("Could not find button event for button on pin " + unsign(buff[0]));
                return;
            }
            event.check(unsign(buff[1]));
        }
    }

    static abstract class ButtonEvent {

        protected long lastTime;
        private long cooldown;

        ButtonEvent(long cooldown) {
            this.cooldown = cooldown;
            lastTime = System.currentTimeMillis();
        }

        void check(int i) {
            if ((System.currentTimeMillis() - lastTime) > getCooldown()) {
                if (i == 1) {
                    onPress();
                    lastTime = System.currentTimeMillis();
                } else onRelease();
            }
        }

        long getCooldown() {
            return cooldown;
        }

        abstract void onPress();
        abstract void onRelease();
    }

    static class HomeButton extends ButtonEvent {

        HomeButton(long cooldown) {
            super(cooldown);
        }

        @Override
        void onPress() {
            //System.out.println("Home button pressed.");
            Overlay.HOME.display(CF.OVERLAY_DURATION, CF.FADE_DURATION, CF.DISPLAY_LOCATION);
            Platform.runLater(() -> Run.engine.loadURL(CF.LOAD_SITE));
        }

        @Override
        void onRelease() {}
    }

    static class ForwardButton extends ButtonEvent {


        ForwardButton(long cooldown) {
            super(cooldown);
        }

        @Override
        void onPress() {
            Platform.runLater(() -> {
                if (Run.engine.canGoForward()) {
                    Overlay.FORWARD.display(CF.OVERLAY_DURATION, CF.FADE_DURATION, CF.DISPLAY_LOCATION);
                    Run.engine.goForward();
                } else Overlay.ERROR.display(CF.OVERLAY_DURATION, CF.FADE_DURATION, CF.DISPLAY_LOCATION);
            });
        }

        @Override
        void onRelease() {}
    }

    static class BackButton extends ButtonEvent {


        BackButton(long cooldown) {
            super(cooldown);
        }

        @Override
        void onPress() {
            Platform.runLater(() -> {
                if (Run.engine.canGoBack()) {
                    Overlay.BACK.display(CF.OVERLAY_DURATION, CF.FADE_DURATION, CF.DISPLAY_LOCATION);
                    Run.engine.goBack();
                } else Overlay.ERROR.display(CF.OVERLAY_DURATION, CF.FADE_DURATION, CF.DISPLAY_LOCATION);

            });
        }

        @Override
        void onRelease() {}
    }
}
