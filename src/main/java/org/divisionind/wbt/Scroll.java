/*
 * Copyright 2017-2018 Division Industries LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.divisionind.wbt;

import javafx.application.Platform;
import org.jnativehook.GlobalScreen;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;
import org.jnativehook.mouse.NativeMouseMotionListener;

import java.awt.*;
import java.util.concurrent.ScheduledFuture;

/**
 * Created by drew6017 on 1/20/2018.
 */
public class Scroll implements Runnable {

    private Point press;
    private int amount;
    private float speed;
    private long polling_time;
    private long last_time;
    private ScheduledFuture<?> smoothScroll;

    public Scroll() {
        this.press = null;
        this.amount = 0;
        this.polling_time = 0;
        this.last_time = System.currentTimeMillis();
    }

    public void start() {
        GlobalScreen.addNativeMouseMotionListener(new NativeMouseMotionListener() {
            @Override
            public void nativeMouseMoved(NativeMouseEvent e) { }

            @Override
            public void nativeMouseDragged(NativeMouseEvent e) {
                if (press != null) {
                    amount = press.y - e.getPoint().y;
                    Platform.runLater(() -> Run.engine.executeJavaScript("window.scrollTo(window.scrollX, window.scrollY + " + amount + ")"));
                    press = e.getPoint();

                    long ctime = System.currentTimeMillis();
                    polling_time = ctime - last_time;
                    last_time = ctime;
                }
            }
        });

        GlobalScreen.addNativeMouseListener(new NativeMouseListener() {
            @Override
            public void nativeMouseClicked(NativeMouseEvent e) { }

            @Override
            public void nativeMousePressed(NativeMouseEvent e) {
                if (e.getButton() == 1 && press == null) {
                    press = e.getPoint(); // left click
                    if (smoothScroll != null) {
                        smoothScroll.cancel(true);
                        smoothScroll = null;
                    }
                    last_time = System.currentTimeMillis();
                }

                if (e.getButton() == 2) {
                    Overlay.HOME.display(CF.OVERLAY_DURATION, CF.FADE_DURATION, CF.DISPLAY_LOCATION);
                    Platform.runLater(() -> Run.engine.loadURL(CF.LOAD_SITE));
                }
            }

            @Override
            public void nativeMouseReleased(NativeMouseEvent e) {
                if (e.getButton() == 1) {
                    press = null;
                    speed = (float)amount;
                    startSmoothScroll();
                }
            }
        });
    }

    private void startSmoothScroll() {
        if (smoothScroll != null) smoothScroll.cancel(true);
        smoothScroll = CF.scheduler.repeating(this, polling_time, polling_time);
    }

    @Override
    public void run() {
        if (Math.abs(speed) < CF.SCROLL_STOP_MAGNITUDE) smoothScroll.cancel(true);
        speed /= CF.SCROLL_SPECTRAL_DECAY;
        Platform.runLater(() -> Run.engine.executeJavaScript("window.scrollTo(window.scrollX, window.scrollY + " + (int)speed + ")"));
    }
}
