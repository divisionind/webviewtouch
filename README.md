# README #

* This is an open source project under the MIT licensing.

### Building Project ###
WebViewTouch uses gradle for it's build process. To setup:

1. Clone the repository.
2. Run gradlew with the arguments "clean fatJar createExe"

### What is this project for? ###

* This project is to act as a generic touch screen web interface for kiosks. It was designed to host a html web interface.

### Contribution guidelines ###

Rules

1. All code must be thoroughly tested and compatible with pre-existing commits.
2. All commits must follow the same coding style as seen throughout the project. e.g:

~~~~
Class {

}
~~~~

not

~~~~
Class
{

}
~~~~

*NOTE: If a commit does not improve performance, add a feature, or correct something that was already intended. It will NOT be accepted.*

### Disclaimers ###

> WebViewTouch uses JxBrowser http://www.teamdev.com/jxbrowser, which is a proprietary software. The use of JxBrowser is governed by the JxBrowser Product Licence Agreement http://www.teamdev.com/jxbrowser-licence-agreement. If you would like to use JxBrowser in your development, please contact TeamDev.

### Libraries ###

WebViewTouch utilizes the following libraries.

* jNativeHook v2.1.0
* jSerialComm v1.3.11
* JxBrowser (proprietary: TeamDev) v6.18/win32
* DivML (proprietary: Division Industries) v0.0.8 https://bitbucket.org/divisionind/divml
* JUnit5